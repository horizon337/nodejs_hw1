const { writeFile, readdir, readFile, stat, unlink } = require('fs')
const path = require('path')
const filesFolder = path.resolve('./files')


const supportedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js']

const createFile = (req, res, next) => {
  try {
    const { filename, content } = req.body;

    if (filename === undefined) {
      res.status(400).json({ message: "Please specify 'filename' parameter" });
      return;
    }

    if (content === undefined) {
      res.status(400).json({ message: "Please specify 'content' parameter" });
      return;
    }

    const fileExtension = path.extname(filename).split('.')[1];

    if (extensions.includes(fileExtension)) {
      fs.writeFileSync(path.join(filesFolder, filename), content, { flag: 'a+' });
      res.status(200).json({ "message": "File created successfully" });
    } else {
      res.status(400).json({
        message: `File extension should be one of the followings: ${supportedExtensions.map((ext) => ext)}`
      });
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: 'Server error' });
  }

};

const getFiles = (req, res, next) => {
  try {
    fs.readdir(filesFolder, (err, files) => {
      if (err) {
        res.status(500).json({ message: 'Server error' });
        return;
      }

      res.status(200).json({
        message: "Success",
        files: files
      });
    });
  } catch (err) {
    console.error(err.message);
    res.status(400).json({ message: 'Client error' });
  }

};

const getFile = (req, res, next) => {
  try {
    const { filename } = req.params;
    const fileExtension = path.extname(filename).split('.')[1];
    const filePath = path.join(filesFolder, filename);

    if (!fs.existsSync(filePath)) {
      res.status(400).json({ message: `No file with '${filename}' filename found` });
      return;
    }

    const { birthtime } = fs.statSync(filePath);
    const content = fs.readFileSync(filePath, 'utf8');

    res.status(200).json({
      message: 'Success',
      filename: filename,
      content: content,
      extension: fileExtension,
      uploadedDate: birthtime
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: 'Server error' });
  }
};


const deleteFile = (req, res, next) => {
  try {
    const { filename } = req.params

    unlink(path.join(filesFolder, filename), (err) => {
      if (err) {
        res.status(400).json({ message: `No file with '${filename}' filename found` })
        return
      }

      res.status(200).send(`${filename} is successfully deleted`)
    })
  } catch (error) {
    res.status(500).json({ message: 'Server error' })
  }
}

const updateFile = (req, res, next) => {
  try {
    const { filename, content } = req.body

    if (filename === undefined) {
      res.status(400).json({ message: "Please specify 'filename' parameter" })
      return
    }

    if (content === undefined) {
      res.status(400).json({ message: "Please specify 'content' parameter" })
      return
    }

    stat(path.join(filesFolder, filename), (err, stats) => {
      if (err) {
        res.status(400).json({ message: `No file with '${filename}' filename found` })
        return
      }

      writeFile(path.join(filesFolder, filename), content, (err) => {
        if (err) {
          res.status(500).json({ message: 'Server error' })
          return
        }

        res.status(200).json({ message: `${filename} is successfully updated` })
      })
    })
  } catch (error) {
    res.status(500).json({ message: 'Server error' })
  }
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  updateFile,
  deleteFile
}
